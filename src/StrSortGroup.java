import java.util.*;

/**
 * 给定一个字符串数组，将字母异位词组合在一起。字母异位词指字母相同，但排列不同的字符串。
 * <p>
 * 示例 1:
 * <p>
 * 输入: strs = ["eat", "tea", "tan", "ate", "nat", "bat"]<p>
 * 输出: [["bat"],["nat","tan"],["ate","eat","tea"]]
 * <p>
 * 示例 2:
 * <p>
 * 输入: strs = [""]<p>
 * 输出: [[""]]
 * <p>
 * 示例 3:
 * <p>
 * 输入: strs = ["a"]<p>
 * 输出: [["a"]]
 * <p>
 * 提示：
 * <p>
 * 1 <= strs.length <= 104<p>
 * 0 <= strs[i].length <= 100<p>
 * strs[i] 仅包含小写字母<p>
 * <p>
 */
public class StrSortGroup {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("输入字符串集合，用逗号隔开：");
        String n = scanner.next();
        String[] split = n.split(",");
//        List<List<String>> returnLists = strSortGroup(split);
        List<List<String>> returnLists = groupAnagrams(split);
        System.out.println(returnLists);
    }

    static List<List<String>> strSortGroup(String[] strings) {
        HashMap<String, List<String>> stringListHashMap = new HashMap<>();
        for (String string : strings) {
            char[] charArray = string.toCharArray();
            Arrays.sort(charArray);
            String sortedStr = Arrays.toString(charArray);
            if (stringListHashMap.containsKey(sortedStr)) {
                stringListHashMap.get(sortedStr).add(string);
            } else {
                List<String> stringArrayList = new ArrayList<>();
                stringArrayList.add(string);
                stringListHashMap.put(sortedStr, stringArrayList);
            }
        }
        return new ArrayList<List<String>>(stringListHashMap.values());
    }

    public static List<List<String>> groupAnagrams(String[] strings) {
        Map<String, List<String>> map = new HashMap<String, List<String>>();
        for (String str : strings) {
            char[] array = str.toCharArray();
            Arrays.sort(array);
            String key = new String(array);
            List<String> list = map.getOrDefault(key, new ArrayList<String>());
            list.add(str);
            map.put(key, list);
        }
        return new ArrayList<List<String>>(map.values());
    }
}
