import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * 给定一个整数数组 numbs 和一个整数目标值 target，请你在该数组中找出 和为目标值 target  的那 两个 整数，并返回它们的数组下标。
 * <p>
 * 示例 1：
 * <p>
 * 输入：numbs = [2,7,11,15], target = 9
 * 输出：[0,1]
 * 解释：因为 numbs[0] + numbs[1] == 9 ，返回 [0, 1] 。
 * <p>
 * 示例 2：
 * <p>
 * 输入：numbs = [3,2,4], target = 6
 * 输出：[1,2]
 * <p>
 * 示例 3：
 * <p>
 * 输入：numbs = [3,3], target = 6
 * 输出：[0,1]
 * <p>
 * 提示：
 * <p>2 <= numbs.length <= 104
 * <p>-109 <= numbs[i] <= 109
 * <p>-109 <= target <= 109
 * <p>只会存在一个有效答案
 * <p>
 *
 * @author Joseph_ZRD_Zhang
 * @date 2023/12/19
 */
public class SumTwoNum {
    /**
     * @param args
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("输入集合，用逗号隔开：");
        String n = scanner.next();
        System.out.println("输入目标：");
        int target = scanner.nextInt();
        String[] split = n.split(",");
        int[] numbs = new int[split.length];
        for (int j = 0; j < split.length; j++) {
            int num = Integer.parseInt(split[j]);
            numbs[j] = num;
        }
        int[] ins1 = twoSum1(numbs, target);
        int[] ins2 = twoSum2(numbs, target);
        System.out.println("构成和为" + target + "的元素下标为：");
        System.out.println(Arrays.toString(ins1));
        System.out.println(Arrays.toString(ins2));
    }

    /**
     * @param numbs
     * @param target
     * @return {@link int[]}
     * <p>
     * 我的题解：sb暴力循环方式
     * O[N²]
     */
    public static int[] twoSum1(int[] numbs, int target) {
        int[] returnSub = new int[2];
        for (int i = 0; i < numbs.length; i++) {
            for (int j = 0; j < numbs.length; j++) {
                if (numbs[i] + numbs[j] == target && i != j) {
                    returnSub[0] = i;
                    returnSub[1] = j;
                }
            }
        }
        return returnSub;
    }

    /**
     * @param numbs
     * @param target
     * @return {@link int[]}
     * <p>
     * 官方题解，通过哈希值解，判断target减去X是否存在于map的key中。
     * 故存储格式为value-----key，没找到就加到map中，下一轮以此类推。
     * O[N]
     */
    public static int[] twoSum2(int[] numbs, int target) {
        Map<Integer, Integer> hashtable = new HashMap<>();
        for (int i = 0; i < numbs.length; ++i) {
            if (hashtable.containsKey(target - numbs[i])) {
                return new int[]{hashtable.get(target - numbs[i]), i};
            }
            hashtable.put(numbs[i], i);
        }
        return new int[0];
    }
}