import java.util.Arrays;
import java.util.Scanner;

/**
 * 给定一个数组 nums，编写一个函数将所有 0 移动到数组的末尾，同时保持非零元素的相对顺序。
 * <p>
 * 请注意 ，必须在不复制数组的情况下原地对数组进行操作。
 * <p>
 * 示例 1:
 * <p>
 * 输入: nums = [0,1,0,3,12]
 * <p>
 * 输出: [1,3,12,0,0]
 * <p>
 * 示例 2:
 * <p>
 * 输入: nums = [0]
 * <p>
 * 输出: [0]
 * <p>
 * 提示:
 * <p>
 * 1 <= nums.length <= 104
 * <p>
 * -231 <= nums[i] <= 231 - 1
 * <p>
 * 进阶：你能尽量减少完成的操作次数吗？
 */
public class MoveZeroes {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("输入集合，用逗号隔开：");
        String n = scanner.next();
        String[] split = n.split(",");
        int[] numbs = new int[split.length];
        for (int j = 0; j < split.length; j++) {
            int num = Integer.parseInt(split[j]);
            numbs[j] = num;
        }
        int[] returnGroup = moveZeroes(numbs);
        System.out.println(Arrays.toString(returnGroup));
    }

    private static int[] moveZeroes(int[] numbs) {
        //两个指针i和j
        int j = 0;
        for (int i = 0; i < numbs.length; i++) {
            //当前元素!=0，就把其交换到左边，等于0的交换到右边
            if (numbs[i] != 0) {
                int tmp = numbs[i];
                numbs[i] = numbs[j];
                numbs[j++] = tmp;
            }
        }
        return numbs;
    }
}
