import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * 给定一个未排序的整数数组 nums ，找出数字连续的最长序列（不要求序列元素在原数组中连续）的长度。
 * <p>
 * 请你设计并实现时间复杂度为 O(n) 的算法解决此问题。
 * <p>
 * 示例 1：
 * <p>
 * 输入：nums = [100,4,200,1,3,2]
 * <p>
 * 输出：4
 * <p>
 * 解释：最长数字连续序列是 [1, 2, 3, 4]。它的长度为 4。
 * <p>
 * 示例 2：
 * <p>
 * 输入：nums = [0,3,7,2,5,8,4,6,0,1]
 * <p>
 * 输出：9
 * <p>
 * 提示：
 * <p>
 * 0 <= nums.length <= 105
 * <p>
 * -109 <= nums[i] <= 109
 * <p>
 */
public class LongestConsecutive {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("输入整数集合，用逗号隔开：");
        String n = scanner.next();
        String[] split = n.split(",");
        int[] numbs = new int[split.length];
        for (int j = 0; j < split.length; j++) {
            int num = Integer.parseInt(split[j]);
            numbs[j] = num;
        }
        int returnInteger = longestConsecutive(numbs);
        System.out.println(returnInteger);
    }

    /**
     * 1.去重
     * <p>
     * 2.如果当前遍历元素-1不存在于set中，则当前元素为连续数中的最小数
     * <p>
     * 3.用while对当前元素进行累加，判断每次累加的结果是否存在于集合中，并记录当前连续个数
     * <p>
     * 4.比较历史最大连续个数和当前连续个数的大小，讲最大值重新赋值给历史最大连续个数
     * <p>
     *
     * @param numbs
     * @return
     */
    private static int longestConsecutive(int[] numbs) {
        Set<Integer> numSet = new HashSet<Integer>();
        for (int num : numbs) {
            numSet.add(num);
        }
        int longestStreak = 0;
        for (int num : numSet) {
            if (!numSet.contains(num - 1)) {
                int currentNum = num;
                int currentStreak = 1;
                while (numSet.contains(currentNum + 1)) {
                    currentNum += 1;
                    currentStreak += 1;
                }
                longestStreak = Math.max(longestStreak, currentStreak);
            }
        }
        return longestStreak;
    }
}
